#!/bin/bash

if [ -z "${1}" ]; then
   version="latest"
else
   version="${1}"
fi


docker push 45.55.18.48:5000/dkmaven/nodejs_app:"${version}"
